syntax = "proto3";

option go_package = "protobuf";

import "google/protobuf/timestamp.proto";
import "google/protobuf/empty.proto";


//Primary structure which holds the
//flat list of parameters. Useful
//for the creation of a new dialpeer in order
//to avoid the complexity of the original structure
message Dialpeer {
	bool enabled = 1;                             //required //false by default because of protobuf
    float connect_fee = 2;                        //required
    float initial_rate = 3; 	                  //required
    int32 initial_interval = 4;                   //required
    int32 next_interval = 5;                      //required
    float next_rate = 6;                          //required
    google.protobuf.Timestamp valid_from = 7;     //required
    google.protobuf.Timestamp valid_till = 8;     //required
    string prefix = 9;
    string src_rewrite_rule = 10;
    string src_rewrite_result = 11;
    string dst_rewrite_rule = 12;
    string dst_rewrite_result = 13;
    float acd_limit = 14;
    float asr_limit = 15;
    bool locked = 16;
    int32 priority = 17;
    bool exclusive_route = 18;
    int32 capacity = 19;
    float lcr_rate_multiplier = 20;
    float force_hit_rate = 21;
    int32 network_prefix_id = 22;
    google.protobuf.Timestamp created_at = 23;
    float short_calls_limit = 24;
    string external_id = 25;
	repeated int32 routing_tag_ids = 26;
	int32 routing_group = 27;                     //required
	int32 vendor = 28;                            //required
	int32 account = 29;                           //required
	int32 gateway = 30;
	int32 gateway_group = 31;
	int32 routing_tag_modes = 32;
}

//Necessary since we have to send
//{"data":{ envelope to the Yeti API
message NewDataEnvelope {
	NewDialpeer data = 1;
}

//Primary structure of the new dialpeer
message NewDialpeer {
    string type = 1;
	DialpeerAttributes attributes = 2;
	NewDialpeerRelationships relationships = 3;
}


message RelationData {
	string type = 1;
	int32 id = 2;
}

message RelationLinks {
	string self = 1;
	string related = 2;
}

message Relation {
	RelationData data = 1;
}

message NewDialpeerRelationships {
	Relation routing_group = 1 [json_name = "routing-group"];
	Relation vendor = 2;
	Relation account = 3;
	Relation gateway = 4;
	Relation gateway_group = 5 [json_name = "gateway-group"];
	//Relation routing_tag_modes = 6 [json_name = "routing-tag-modes"];
}

//Necessary because the server response with
//{"data":{ envelope
message ExistingDataEnvelope {
	ExistingDialpeer data = 1;
}

//Primary structure of the existing dialpeer
message ExistingDialpeer {
    string type = 1;
    DialpeerAttributes attributes = 2;
    string id = 3;

}

message ListOfExistingDialpeers {
	repeated ExistingDialpeer data = 1;
}

message DialpeerAttributes {
    bool enabled = 1;
    float connect_fee = 2 [json_name = "connect-fee"];
    float initial_rate = 3 [json_name = "initial-rate"];
    int32 initial_interval = 4 [json_name = "initial-interval"];
    int32 next_interval = 5 [json_name = "next-interval"];
    float next_rate = 6 [json_name = "next-rate"];
    google.protobuf.Timestamp valid_from = 7 [json_name = "valid-from"];
    google.protobuf.Timestamp valid_till = 8 [json_name = "valid-till"]; 
    string prefix = 9;
    string src_rewrite_rule = 10 [json_name = "src-rewrite-rule"];
    string src_rewrite_result = 11 [json_name = "src-rewrite-result"];
    string dst_rewrite_rule = 12 [json_name = "dst-rewrite-rule"];
    string dst_rewrite_result = 13 [json_name = "dst-rewrite-result"];
    float acd_limit = 14 [json_name = "acd-limit"];
    float asr_limit = 15 [json_name = "asr-limit"];
    bool locked = 16;
    int32 priority = 17;
    bool exclusive_route = 18 [json_name = "exclusive-route"];
    int32 capacity = 19;
    float lcr_rate_multiplier = 20 [json_name = "lcr-rate-multiplier"];
    float force_hit_rate = 21 [json_name = "force-hit-rate"];
    int32 network_prefix_id = 22 [json_name = "network-prefix-id"];
    google.protobuf.Timestamp created_at = 23 [json_name = "created-at"];
    float short_calls_limit = 24 [json_name = "short-calls-limit"];
    string external_id = 25 [json_name = "external-id"];
    repeated int32 routing_tag_ids = 26 [json_name = "routing-tag-ids"];
}

message DialpeerNextRate {
    string type = 1; //Resource type (dialpeers-next-rates)
}

message DialpeerNextRateAttributes {
    float next_rate = 1 [json_name = "next-rate"];
    float initial_rate = 2 [json_name = "initial-rate"];
    int32 initial_interval = 3 [json_name = "initial-interval"];
    int32 next_interval = 4 [json_name = "next-interval"];
    float connect_fee = 5 [json_name = "connect-fee"];
    google.protobuf.Timestamp apply_time = 6 [json_name = "apply-time"];
    bool applied = 7;
    string external_id = 8;
}

message DialpeerNextRateRelationships {
	Relation dialpeer = 1;
}

message ListOfDialpeerNextRates {
    repeated DialpeerNextRate data = 1;
}

//Filters structure is useful for search requests
//you can use either one or all parameters
message Filters {
	string prefix = 1;
	string external_id = 2;
	string routing_group_id = 3;
}

//Version
message Version {
    string id = 1;
}

//All remote procedures which are currently supported by the service
service Yeti {
	rpc GetDialpeerByID(ExistingDialpeer) returns (ExistingDialpeer);
	rpc GetDialpeerByFilter(Filters) returns (stream ExistingDialpeer);
    rpc CreateDialpeer(Dialpeer) returns (ExistingDialpeer);
    rpc UpdateDialpeer(ExistingDialpeer) returns (ExistingDialpeer);

    rpc CreateDialpeerNextRate(DialpeerNextRate) returns (DialpeerNextRate);
    rpc GetDialpeerNextRate(DialpeerNextRate) returns (DialpeerNextRate);
    rpc ListDialpeerNextRate(DialpeerNextRate) returns (stream DialpeerNextRate);
    rpc UpdateDialpeerNextRate(DialpeerNextRate) returns (DialpeerNextRate);
    rpc DeleteDialpeerNextRate(DialpeerNextRate) returns (DialpeerNextRate);

    rpc ShowVersion(google.protobuf.Empty) returns (Version);
}
